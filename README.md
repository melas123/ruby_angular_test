


Installation
---------------
Before starting,  check if you have ruby, rails, git and bower installed on your machine
  * Clone this repo
  * run `bundle install`
  * run `rake db:setup `

  install dependencies (angular, bootstrap, ...)
  * `bower install`


Server
--------------
  run `rails server`

Enjoy at [http://localhost:3000/#/login](http://localhost:3000/#/login)



To create other Users [http://localhost:3000/#/register](http://localhost:3000/#/register)




