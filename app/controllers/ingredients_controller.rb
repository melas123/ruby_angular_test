class IngredientsController < ApplicationController
  respond_to :json
  def index
    @ingredients = if params[:keywords]
                 Ingredient.where("title like ?","%#{params[:keywords]}%")
               else
                 Ingredient.all
               end
  end

  def create
    if params[:recipe]
    @recipe = Recipe.new(params.require(:recipe).permit(:name,:instructions,:cover,:ingredients))
    else
    @ingredient = Ingredient.new(params.require(:ingredient).permit(:title))
    @ingredient.save
    render 'show', status: 201
     end
  end
end
