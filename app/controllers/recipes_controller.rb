class RecipesController < ApplicationController
  before_action :authenticate_user! , except: [:index]
  #before_filter :process_params, only: [:create, :update]
  Encoding.default_external = Encoding::UTF_8
  respond_to :json
  def index
    page = params[:page].present? ? params[:page] : 1
    @recipes = if params[:keywords]
                 Recipe.where("name like ?","%#{params[:keywords]}%")
               else
                 @recipes = Recipe.all.order('created_at DESC').paginate(:page => page, :per_page => 10)
                   #byebug
                 render :json => Paginator.pagination_attributes(@recipes).merge!(:recipes => @recipes)
               end
  end

  #def index
   # @recipes = Recipe.all
  #end

  def new
    @recipe = Recipe.new
    @ingredients = Ingredient.all

  end

  def show
    @recipe = Recipe.find(params[:id])
  end


  def create



    #@ingredient = Ingredient.find(params[:ingredients][:id])

    #@ingredients = Ingredient.where(:id => params[:recipe][:ingredients].map {|ingredient| ingredient[:id]})
    #@recipe = Recipe.new(params.require(:recipe).permit(:name,:instructions,:cover,:user_id, :ingredients))
    #params[:ingredients].each_pair do |k,v|

    #end
    ##@recipe = current_user.recipes.new(params.require(:recipe).permit(:name,:instructions ))
    params[:recipe] = JSON.parse params[:recipe]
    #params[:recipe][:cover] = Hash.new;

    params[:recipe][:cover] = params[:cover]
    #puts @recipe.cover
    @ingredients = Ingredient.where(:id => params[:recipe][:ingredients].map {|ingredient| ingredient[:id]})
    #@ingredients = Ingredient.where(:id => params[:id])

    @recipe = current_user.recipes.new(params.require(:recipe).permit(:name,:instructions,:cover))

    puts @recipe.cover
    #if @recipe.cover = params[:recipe][:cover]
     # @recipe.cover = ActionDispatch::Http::UploadedFile.new(tempfile: "cover", filename: File.basename("7.jpg"), type: "image/jpeg")
      #puts @recipe.cover
    @recipe.ingredients << @ingredients
    #@recipe.cover = params[:cover]
    @recipe.user_id = current_user.id
    #byebug
    @recipe.save
    render 'index', status: 201
   # end
  end

  def update
    recipe = Recipe.find(params[:id])
    recipe.update_attributes(params.require(:recipe).permit(:name,:instructions,:cover))
    head :no_content
  end

  def destroy
    recipe = Recipe.find(params[:id])
    recipe.destroy
    head :no_content
  end

  def recipe_params
    params.require(:recipe).permit(:name,:instructions,:cover,:user_id, {ingredients:  [:id, :title]})
  end


  def process_params
    params[:recipe] = JSON.parse(params[:recipe])
                               .with_indifferent_access
    if params[:cover]
      params[:recipe][:cover] = params[:cover]
    else
      render "error"
    end

  end

end
