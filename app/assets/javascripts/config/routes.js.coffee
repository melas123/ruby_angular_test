@cooking.config ( $routeProvider, flashProvider ) ->

  $routeProvider

  .when "/login",
    controller:  'authCtrl'
    templateUrl: 'assets/templates/auth/_login.html'

  .when "/register",
    controller:  'authCtrl'
    templateUrl: 'assets/templates/auth/_register.html'

  .when "/recipes/new",
    controller:  'recipeCtrl'
    templateUrl: 'assets/templates/form.html'

  .when "/recipes/:recipeId/edit",
    controller:  'recipeCtrl'
    templateUrl: 'assets/templates/form.html'

  .when "/recipes/:recipeId",
    controller:  'recipeCtrl'
    templateUrl: 'assets/templates/show.html'

  .when "/home",
    controller:  'homeCtrl'
    templateUrl: 'assets/templates/home/index.html'

  .otherwise redirectTo: "/home"


  flashProvider.errorClassnames.push("alert-danger")
  flashProvider.warnClassnames.push("alert-warning")
  flashProvider.infoClassnames.push("alert-info")
  flashProvider.successClassnames.push("alert-success")