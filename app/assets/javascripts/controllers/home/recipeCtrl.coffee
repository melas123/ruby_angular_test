@cooking.controller 'recipeCtrl', [
  '$scope'
  'Recipes'
  '$location'
  '$routeParams'
  '$resource'
  'flash'
  '$rootScope'
  'Auth'
  'Ingredients'
  '$timeout'
  'FileUploader'

  ($scope, Recipes, $location, $routeParams, $resource, flash, $rootScope, Auth, Ingredients, $timeout, FileUploader) ->
    #console.log(Auth.currentUser())
   # $scope.uploader = new FileUploader({
    #  url: '/recipes.json',
     # method: 'POST'
    #});
    csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    $scope.expression=5
    uploader = $scope.uploader = new FileUploader(
      url: '/recipes',
      alias:  'cover',
      removeAfterUpload:  true,
      #transformRequest: angular.identity,
      headers: {'X-CSRF-TOKEN': csrf_token,'request': 'application/json','accept': 'application/json'},
      withCredentials: true
    )
    Recipe = $resource('/recipes/:recipeId', { recipeId: "@id", format: 'json' },
      {
        'save':   {method:'PUT'},
        'create': {method:'POST', isArray: false}
      }
    )
    Ingredient = $resource('/ingredients/:ingredientId', { ingredientId: "@id", format: 'json' },
      {
        'save':   {method:'PUT'},
        'create': {method:'POST', isArray: false}
      }
    )
    Test = $resource('/recipes/:recipeId/ingredients/',{ recipeId: "@id", format: 'json' },
      {
        'save':   {method:'PUT'},
        'create': {method:'POST', isArray: false}
      }


    if $routeParams.recipeId

      #$scope.recipe = $resource('/recipes/:recipeId', { recipeId: $routeParams.recipeId, format: 'json'})
      Recipe.get({recipeId: $routeParams.recipeId},
        ( (recipe)-> $scope.recipe = recipe ),
        ( (httpResponse)->
          $scope.recipe = null
          flash.error   = "There is no recipe with ID #{$routeParams.recipeId}"
        )
      )
    else
      $scope.recipe = {}

    $scope.back   = -> $location.path("/home")
    $scope.edit   = -> $location.path("/recipes/#{$scope.recipe.id}/edit")
    $scope.cancel = ->
      if $scope.recipe.id
        $location.path("/recipes/#{$scope.recipe.id}")
      else
        $location.path("/home")

    $scope.save = ->

      onError = (_httpResponse)-> flash.error = "Something went wrong"
      if $scope.recipe.id
        console.log("Test number 2")   
        $scope.recipe.$save(

          #$resource('/recipes/:recipeId/ingredients', { recipeId: "@id", format: 'json' }
          ( ()-> $location.path("/recipes/#{$scope.recipe.id}") ),
          onError)

      else
        uploader.onBeforeUploadItem = (item)->
          #data = angular.toJSON($scope.recipe)
          item.formData.push("recipe": angular.toJson($scope.recipe))
          #item.upload()
          console.info('uploader', $scope.uploader);
        uploader.uploadAll()
        #uploader.onCompleteAll()
        $location.path("/home")
        console.info('uploader', $scope.uploader);

        )




    $scope.delete = ->
      $scope.recipe.$delete()
      $scope.back()
#this is for the ingredients


    $scope.ingredients = Ingredients.query()

    $scope.createNewIngredient = ->
      $scope.ingredients.push($scope.ingredient)
      Ingredient.create($scope.ingredient)

    $scope.uploadFiles = (file, errFiles) ->
      $scope.f = file
      $scope.errFile = errFiles and errFiles[0]
      if file
        file.upload = Upload.upload(
          url: 'https://angular-file-upload-cors-srv.appspot.com/upload'
          data: file: file)
      file.upload.then ((response) ->
        $timeout ->
          file.result = response.data

      ), ((response) ->
        if response.status > 0
          $scope.errorMsg = response.status + ': ' + response.data

      ), (evt) ->
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))


      console.info('uploader', uploader);

     


]
###$scope.uploader.onBeforeUploadItem = onBeforeUploadItem;
onBeforeUploadItem = (item) ->
  item.formData.push recipe: $scope.recipe
  console.log item
  return###
#$resource('/ingredients/:recipeId', { recipeId: "@id", format: 'json' }

