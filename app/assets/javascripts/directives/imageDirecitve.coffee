@cooking.directive 'uploadImage', ->
return{
restrict: 'A'
link:(scope,elem)->
  reader = new FileReader()
  reader.onload =(e)->
    scope.iFile = btoa(e.target.result)
    scope.$apply()

  elem.on 'change', ->
    scope.iFile=''
    file = elem[0].files[0]
    scope.iFilesize = file.size
    scope.iFiletype = file.type
    scope.iFilename = file.name
    scope.$apply()
    reader.readAsBinaryString(file)
}