# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@cooking = angular.module( 'cookingRecipes', ['Devise', 'ngRoute', 'ngResource', 'templates', 'angular-flash.service',
                                              'angular-flash.flash-alert-directive','angularFileUpload'
                                               , 'ngSanitize','ui.select','bgf.paginateAnything'] )



@cooking.factory 'Recipes', [
  '$resource'
  ($resource,page) ->
    $resource '/recipes.json', {},
      query:
        method: 'GET'
        params: page: '@page'



]

@cooking.factory 'Ingredients', [
  '$resource'
  ($resource) ->
    $resource '/ingredients.json', {},
      query:
        method: 'GET'
        isArray: true
      create:
        method: 'POST'
        isArray: true
      search:
        method: 'GET'
        params: keywords: '@keywords'
        isArray: true
]

@cooking.factory 'Recipe', [
  '$resource'
  ($resource) ->
    $resource '/recipes/:id.json', {},
      show:
        method: 'GET'
        params: id: '@id'
      update:
        method: 'PUT'
        params: id: '@id'
      delete:
        method: 'DELETE'
        params: id: '@id'
  ]

@cooking.directive 'myDirective', (httpPostFactory) ->
  {
  restrict: 'A'
  scope: true
  link: (scope, element, attr) ->
    element.bind 'change', ->
      formData = new FormData
      formData.append 'file', element[0].files[0]
      httpPostFactory 'upload_image.php', formData, (callback) ->
        console.log callback
        return
      return
    return

  }

  @cooking.factory 'httpPostFactory', ($http) ->
  (file, data, callback) ->
    $http(
      url: file
      method: 'POST'
      data: data
      headers: 'Content-Type': undefined).success (response) ->
    callback response
    return
    return
