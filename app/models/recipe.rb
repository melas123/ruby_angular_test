class Recipe < ActiveRecord::Base

  mount_uploader :cover, AvatarUploader
  belongs_to :user, :foreign_key => :user_id

  has_and_belongs_to_many :ingredients,:join_table => "ingredients_recipes"

  accepts_nested_attributes_for :ingredients
  #attr_accessor :cover

  def as_json(options = {})
    super(options.merge(include:{
                            ingredients: {only: [:name, :id]},
                            user: {only: [:id, :name]}
                        }))
  end
end
