Rails.application.routes.draw do
  get 'user/index'

  #get 'recipes/index'
  resources :recipes, only: [:index, :show, :create, :update, :destroy, :get, :search, :list] do
    resources :ingredients

  end
  #resources :ingredients, path: '/recipes/ingredients'
  #resources :recipes
  resources :ingredients
  post '/recipes/ingredients/', to: 'recipes#create', as: :create_recipe
  devise_for :users, :controllers => { registrations: 'registrations' }
  root to: 'application#angular'
  resources :users
end
