class AddCoverFilenameToRecipes < ActiveRecord::Migration
  def change
    add_column :recipes, :cover_filename, :string
  end
end
