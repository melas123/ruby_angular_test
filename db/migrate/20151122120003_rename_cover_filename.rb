class RenameCoverFilename < ActiveRecord::Migration
  def change
    rename_column :recipes, :cover_filename, :cover

  end
end
