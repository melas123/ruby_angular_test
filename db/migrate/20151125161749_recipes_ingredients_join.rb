class RecipesIngredientsJoin < ActiveRecord::Migration
  def self.up
    create_table 'ingredients_recipes', :id => false do |t|
      t.column 'ingredient_id', :integer
      t.column 'recipe_id', :integer
    end

  end

  def self.down
    drop_table 'ingredients_recipes'

  end
end
